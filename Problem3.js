// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. 
//Execute a function to Sort all the car model names into alphabetical order 
//and log the results in the console as it was returned.

function carModel (inventory) {
    let outputArray = [];
    for (let index = 0; index < inventory.length; index++) {
        outputArray.push (inventory[index].car_model);
    }
    return string_sort (outputArray);
}

function string_sort(str) {
    let i = 0, j;
    while (i < str.length) {
        j = i + 1;
        while (j < str.length) {
            if (str[j] < str[i]) {
                var temp = str[i];
                str[i] = str[j];
                str[j] = temp;
            }
            j++;
        }
        i++;
    }
    return str;
}

module.exports = carModel;
