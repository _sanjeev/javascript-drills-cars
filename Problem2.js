
// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory. 
//Execute a function to find what the make and model of the last car in the 
//inventory is?  Log the make and model into the console in the format of: 

function lastCar (inventory) {
    const lastIndex = inventory.length - 1;
    if (lastIndex === inventory.length - 1) {
        return 'Car Make : ' + inventory[lastIndex].car_make + ', Car Model : ' + inventory[lastIndex].car_model;
    }
}

module.exports = lastCar;