const inventory = require('./inventory.js');
const carModel = require('./Problem3.js');
let actual = carModel(inventory);
console.log(actual);

const expected = [
    '300M', '4000CS Quattro', '525',
    '6 Series', 'Accord', 'Aerio',
    'Bravada', 'Camry', 'Cavalier',
    'Ciera', 'Defender Ice Edition', 'E-Class',
    'Econoline E250', 'Escalade', 'Escort',
    'Esprit', 'Evora', 'Express 1500',
    'Familia', 'Fortwo', 'G35',
    'GTO', 'Galant', 'Intrepid',
    'Jetta', 'LSS', 'MR2',
    'Magnum', 'Miata MX-5', 'Montero Sport',
    'Mustang', 'Navigator', 'Prizm',
    'Q', 'Q7', 'R-Class',
    'Ram Van 1500', 'Ram Van 3500', 'Sebring',
    'Skylark', 'TT', 'Talon',
    'Topaz', 'Town Car', 'Windstar',
    'Wrangler', 'Wrangler', 'XC70',
    'Yukon', 'riolet'
]
let equal = true;

for (let index = 0; index < actual.length; index++) {
    if (actual[index] !== expected[index]) {
        equal = false;
        console.log('Different');
        break;
    }
}
if (equal) {
    console.log('Same');
}

