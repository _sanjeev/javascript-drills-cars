const inventory = require('./inventory.js');
const carYear = require('./Problem5.js');

let actual = carYear(inventory);
let ans = inventory.length - actual.length;
console.log('Number of car Before 2000 :- ' + ans);
console.log(actual);

const expected = [
    1983, 1990, 1995, 1987, 1996,
    1997, 1999, 1987, 1995, 1994,
    1985, 1997, 1992, 1993, 1964,
    1999, 1991, 1997, 1992, 1998,
    1965, 1996, 1995, 1996, 1999
];

let equal = true;

for (let index = 0; index < expected.length; index++) {
    if (actual[index] !== expected[index]) {
        console.log ('Different');
        equal = false;
        break;
    }
}

if (equal) {
    console.log ('Equal');
}