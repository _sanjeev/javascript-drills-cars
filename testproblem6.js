const inventory = require('./inventory.js');
const buyAudiBmw = require('./Problem6.js');

let actual = buyAudiBmw(inventory);
console.log(actual);
const expected = ['Audi', 'Audi', 'BMW', 'BMW', 'Audi', 'Audi'];

let equal = true;

for (let index = 0; index < expected.length; index++) {
    if (expected[index] !== actual[index]) {
        console.log('Different');
        equal = false;
        break;
    }
}

if (equal === true) {
    console.log ('Same');
}